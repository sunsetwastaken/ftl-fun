#include<iostream>
#include<Windows.h>
#include<Psapi.h>

#define MAX_HULL 30

using namespace std;

HANDLE GetHandle() {
	HWND hwnd = FindWindowA(NULL, "FTL: Faster Than Light");
	if (hwnd) {
		DWORD procID;
		GetWindowThreadProcessId(hwnd, &procID);
		HANDLE hProcHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE , procID);
		if (procID) {
			return hProcHandle;
		}
		else {
			cout << "can't get procID";
			Sleep(3000);
			exit(-1);
		}
	}
	else {
		cout << "cannot find window";
		Sleep(3000);
		exit(-1);
	}
}

HMODULE GetModule() {
	HANDLE pHandle = GetHandle();
	HMODULE hMods[1024];
	DWORD cbNeeded;
	unsigned int i;
	if (EnumProcessModules(pHandle, hMods, sizeof(hMods), &cbNeeded)) {
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++) {
			TCHAR szModName[MAX_PATH];
			if (GetModuleFileNameEx(pHandle, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR))) {
				wstring wstrModName = szModName;
				wstring wstrModContain = L"FTLGame.exe"; 
				if (wstrModName.find(wstrModContain) != string::npos) {
					//not really sure why stackoverflow guy wanted to close the handle here cuz you just have to make a new one later but whatever
					CloseHandle(pHandle);
					return hMods[i];
				}
			}
		}

	}
}

VOID PrintData(DWORD baseAddr, DWORD basePointer, DWORD shipBase, DWORD hullOffset, DWORD scrapOffset, DWORD fuelOffset, DWORD dronesOffset, DWORD missileOffset) {
	// to look nice could format so all are 0x00000000 instead of random lengths
	cout << "FTL Data" << endl
		<< "*********\n" << endl
		<< "Base Address" << endl
		<< "FTLGame.exe + 53D38C" << endl
		<< hex << baseAddr
		<< " + 53D38C\n" << endl
		<< "Base Pointer" << endl
		<< hex << basePointer << endl
		<< "\nShip Base" << endl
		<< hex << shipBase << endl
		<< "\nVariable Addresses" << endl
		<< "Hull - " 
		<< hex << (shipBase + hullOffset) << endl
		<< "Scrap - " 
		<< hex << (shipBase + scrapOffset) << endl
		<< "Fuel - " 
		<< hex << (shipBase + fuelOffset) << endl
		<< "Drones - "
		<< hex << (shipBase + dronesOffset) << endl
		<< "Missiles - "
		<< hex << (shipBase + missileOffset) << endl
		<< "*********\n";
}

int main() {
	//if nothing else i know more than i knew yesterday :D

	//ftlgame.exe + 53D38C
	DWORD baseAddr = 0x0;
	DWORD basePointer = 0x0;
	DWORD shipBase = 0x0;

	//following variables are offsets and never change
	CONST DWORD hullOffset = 0xCC;
	CONST DWORD scrapOffset = 0x4D4;
	CONST DWORD fuelOffset = 0x494;
	CONST DWORD dronesOffset = 0x800;

	// probs broken
	CONST DWORD missileOffset = 0x1E8;

	//following variables assigned through readproccessmemory
	int hullCount = 30;
	int fuelCount;
	int missileCount;
	int dronesCount;
	int scrapCount;

	//temp variables for comparison
	int tempHullCount;
	int tempFuelCount;
	int tempMissileCount;
	int tempDronesCount;
	int tempScrapCount;

	//modified by key presses in ftl
	BOOL infHull = 0;
	BOOL infScrap = 0;
	BOOL infFuel = 0;
	BOOL infDrones = 0;
	BOOL infMissiles = 0;

	//main
	HMODULE Module = GetModule();
	baseAddr = (DWORD)Module;
	
	//get a new handle cuz we closed it after finding the base address for some reason
	HANDLE Handle = GetHandle();

	basePointer = baseAddr + 0x53D38C;
	ReadProcessMemory(Handle, (PBYTE*)basePointer, &shipBase, sizeof(DWORD), 0);

	//print address data for debugging or whatever
	PrintData(baseAddr, basePointer, shipBase, hullOffset, scrapOffset, fuelOffset, dronesOffset, missileOffset);

	//actually doing useful game stuff

	//pressing F8 should kill the program
	while (!GetAsyncKeyState(VK_F8)) {

		if (infHull) {
			//with infHull the hullCount shouldn't be able to be lowered so it remembers what the value was in hullCount and keeps checking for updates to the value, storing these in tempHullCount, if they're lowered write hullCount back to memory
			ReadProcessMemory(Handle, (PBYTE*)(shipBase + hullOffset), &tempHullCount, sizeof(int), 0);
			if (tempHullCount < hullCount) {
				WriteProcessMemory(Handle, (LPVOID)(shipBase + hullOffset), &hullCount, sizeof(hullCount), 0);
			}
			//you could do this for else but then it would constantly be re-writing variables that aren't changing which just seems really bad, but are constant comparisons better?
			else if (tempHullCount > hullCount) {
				hullCount = tempHullCount;
			}
		}
		else {
			//constantly reads vaules so it can pause the value at its most recent
			ReadProcessMemory(Handle, (PBYTE*)(shipBase + hullOffset), &hullCount, sizeof(int), 0);
		}

		if (infScrap) {
			ReadProcessMemory(Handle, (PBYTE*)(shipBase + scrapOffset), &tempScrapCount, sizeof(int), 0);
			if (tempScrapCount < scrapCount) {
				WriteProcessMemory(Handle, (LPVOID)(shipBase + scrapOffset), &scrapCount, sizeof(scrapCount), 0);
			}
			else if (tempScrapCount > scrapCount) {
				scrapCount = tempScrapCount;
			}
		}
		else {
			ReadProcessMemory(Handle, (PBYTE*)(shipBase + scrapOffset), &scrapCount, sizeof(int), 0);
		}

		if (GetAsyncKeyState(VK_F1)) {
			infHull = !infHull;
			cout << "F1 : INFINITE HULL : "
				<< infHull << endl;
		}

		if (GetAsyncKeyState(VK_F2)) {
			infScrap = !infScrap;
			cout << "F2 : INFINITE SCRAP : "
				<< infScrap << endl;
		}

		//end of loop wait a bit cuz idk dont wanna use alot of resources i guess?
		Sleep(200);
	}

	//apparently you were supposed to close them after opening whoopsies
	CloseHandle(Handle);
	Sleep(1000);
	exit(0);
}